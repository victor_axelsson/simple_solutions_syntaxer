﻿namespace Syntaxer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_console = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_endWith = new System.Windows.Forms.TextBox();
            this.txt_beginWith = new System.Windows.Forms.TextBox();
            this.btn_clearConsole = new System.Windows.Forms.Button();
            this.cbox_append = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txt_key1 = new System.Windows.Forms.TextBox();
            this.txt_val1 = new System.Windows.Forms.TextBox();
            this.txt_val2 = new System.Windows.Forms.TextBox();
            this.txt_key2 = new System.Windows.Forms.TextBox();
            this.txt_val3 = new System.Windows.Forms.TextBox();
            this.txt_key3 = new System.Windows.Forms.TextBox();
            this.txt_val4 = new System.Windows.Forms.TextBox();
            this.txt_key4 = new System.Windows.Forms.TextBox();
            this.txt_val5 = new System.Windows.Forms.TextBox();
            this.txt_key5 = new System.Windows.Forms.TextBox();
            this.txt_val6 = new System.Windows.Forms.TextBox();
            this.txt_key6 = new System.Windows.Forms.TextBox();
            this.txt_val7 = new System.Windows.Forms.TextBox();
            this.txt_key7 = new System.Windows.Forms.TextBox();
            this.txt_val8 = new System.Windows.Forms.TextBox();
            this.txt_key8 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_clearValues = new System.Windows.Forms.Button();
            this.btn_clearKeys = new System.Windows.Forms.Button();
            this.txt_template = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sQLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Template";
            // 
            // txt_console
            // 
            this.txt_console.Location = new System.Drawing.Point(15, 304);
            this.txt_console.Name = "txt_console";
            this.txt_console.Size = new System.Drawing.Size(702, 170);
            this.txt_console.TabIndex = 2;
            this.txt_console.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 288);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Console";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txt_endWith);
            this.groupBox1.Controls.Add(this.txt_beginWith);
            this.groupBox1.Controls.Add(this.btn_clearConsole);
            this.groupBox1.Controls.Add(this.cbox_append);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Location = new System.Drawing.Point(331, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(174, 228);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Toolbar";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "End row with";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Begin row with";
            // 
            // txt_endWith
            // 
            this.txt_endWith.Location = new System.Drawing.Point(7, 142);
            this.txt_endWith.Name = "txt_endWith";
            this.txt_endWith.Size = new System.Drawing.Size(162, 20);
            this.txt_endWith.TabIndex = 5;
            // 
            // txt_beginWith
            // 
            this.txt_beginWith.Location = new System.Drawing.Point(6, 103);
            this.txt_beginWith.Name = "txt_beginWith";
            this.txt_beginWith.Size = new System.Drawing.Size(162, 20);
            this.txt_beginWith.TabIndex = 4;
            // 
            // btn_clearConsole
            // 
            this.btn_clearConsole.Location = new System.Drawing.Point(6, 199);
            this.btn_clearConsole.Name = "btn_clearConsole";
            this.btn_clearConsole.Size = new System.Drawing.Size(162, 23);
            this.btn_clearConsole.TabIndex = 3;
            this.btn_clearConsole.Text = "Clear console";
            this.btn_clearConsole.UseVisualStyleBackColor = true;
            this.btn_clearConsole.Click += new System.EventHandler(this.btn_clearConsole_Click);
            // 
            // cbox_append
            // 
            this.cbox_append.AutoSize = true;
            this.cbox_append.Location = new System.Drawing.Point(7, 53);
            this.cbox_append.Name = "cbox_append";
            this.cbox_append.Size = new System.Drawing.Size(63, 17);
            this.cbox_append.TabIndex = 2;
            this.cbox_append.Text = "Append";
            this.cbox_append.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(6, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(162, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Generate";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txt_key1
            // 
            this.txt_key1.Location = new System.Drawing.Point(511, 62);
            this.txt_key1.Name = "txt_key1";
            this.txt_key1.Size = new System.Drawing.Size(100, 20);
            this.txt_key1.TabIndex = 5;
            // 
            // txt_val1
            // 
            this.txt_val1.Location = new System.Drawing.Point(617, 62);
            this.txt_val1.Name = "txt_val1";
            this.txt_val1.Size = new System.Drawing.Size(100, 20);
            this.txt_val1.TabIndex = 6;
            // 
            // txt_val2
            // 
            this.txt_val2.Location = new System.Drawing.Point(617, 91);
            this.txt_val2.Name = "txt_val2";
            this.txt_val2.Size = new System.Drawing.Size(100, 20);
            this.txt_val2.TabIndex = 8;
            // 
            // txt_key2
            // 
            this.txt_key2.Location = new System.Drawing.Point(511, 91);
            this.txt_key2.Name = "txt_key2";
            this.txt_key2.Size = new System.Drawing.Size(100, 20);
            this.txt_key2.TabIndex = 7;
            // 
            // txt_val3
            // 
            this.txt_val3.Location = new System.Drawing.Point(617, 117);
            this.txt_val3.Name = "txt_val3";
            this.txt_val3.Size = new System.Drawing.Size(100, 20);
            this.txt_val3.TabIndex = 10;
            // 
            // txt_key3
            // 
            this.txt_key3.Location = new System.Drawing.Point(511, 117);
            this.txt_key3.Name = "txt_key3";
            this.txt_key3.Size = new System.Drawing.Size(100, 20);
            this.txt_key3.TabIndex = 9;
            // 
            // txt_val4
            // 
            this.txt_val4.Location = new System.Drawing.Point(617, 143);
            this.txt_val4.Name = "txt_val4";
            this.txt_val4.Size = new System.Drawing.Size(100, 20);
            this.txt_val4.TabIndex = 12;
            // 
            // txt_key4
            // 
            this.txt_key4.Location = new System.Drawing.Point(511, 143);
            this.txt_key4.Name = "txt_key4";
            this.txt_key4.Size = new System.Drawing.Size(100, 20);
            this.txt_key4.TabIndex = 11;
            // 
            // txt_val5
            // 
            this.txt_val5.Location = new System.Drawing.Point(617, 169);
            this.txt_val5.Name = "txt_val5";
            this.txt_val5.Size = new System.Drawing.Size(100, 20);
            this.txt_val5.TabIndex = 14;
            // 
            // txt_key5
            // 
            this.txt_key5.Location = new System.Drawing.Point(511, 169);
            this.txt_key5.Name = "txt_key5";
            this.txt_key5.Size = new System.Drawing.Size(100, 20);
            this.txt_key5.TabIndex = 13;
            // 
            // txt_val6
            // 
            this.txt_val6.Location = new System.Drawing.Point(617, 195);
            this.txt_val6.Name = "txt_val6";
            this.txt_val6.Size = new System.Drawing.Size(100, 20);
            this.txt_val6.TabIndex = 16;
            // 
            // txt_key6
            // 
            this.txt_key6.Location = new System.Drawing.Point(511, 195);
            this.txt_key6.Name = "txt_key6";
            this.txt_key6.Size = new System.Drawing.Size(100, 20);
            this.txt_key6.TabIndex = 15;
            // 
            // txt_val7
            // 
            this.txt_val7.Location = new System.Drawing.Point(617, 221);
            this.txt_val7.Name = "txt_val7";
            this.txt_val7.Size = new System.Drawing.Size(100, 20);
            this.txt_val7.TabIndex = 18;
            // 
            // txt_key7
            // 
            this.txt_key7.Location = new System.Drawing.Point(511, 221);
            this.txt_key7.Name = "txt_key7";
            this.txt_key7.Size = new System.Drawing.Size(100, 20);
            this.txt_key7.TabIndex = 17;
            // 
            // txt_val8
            // 
            this.txt_val8.Location = new System.Drawing.Point(617, 248);
            this.txt_val8.Name = "txt_val8";
            this.txt_val8.Size = new System.Drawing.Size(100, 20);
            this.txt_val8.TabIndex = 20;
            // 
            // txt_key8
            // 
            this.txt_key8.Location = new System.Drawing.Point(511, 248);
            this.txt_key8.Name = "txt_key8";
            this.txt_key8.Size = new System.Drawing.Size(100, 20);
            this.txt_key8.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(512, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Key";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(617, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Value";
            // 
            // btn_clearValues
            // 
            this.btn_clearValues.Location = new System.Drawing.Point(617, 275);
            this.btn_clearValues.Name = "btn_clearValues";
            this.btn_clearValues.Size = new System.Drawing.Size(99, 23);
            this.btn_clearValues.TabIndex = 23;
            this.btn_clearValues.Text = "Clear Values";
            this.btn_clearValues.UseVisualStyleBackColor = true;
            this.btn_clearValues.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_clearKeys
            // 
            this.btn_clearKeys.Location = new System.Drawing.Point(511, 274);
            this.btn_clearKeys.Name = "btn_clearKeys";
            this.btn_clearKeys.Size = new System.Drawing.Size(100, 23);
            this.btn_clearKeys.TabIndex = 24;
            this.btn_clearKeys.Text = "Clear keys";
            this.btn_clearKeys.UseVisualStyleBackColor = true;
            this.btn_clearKeys.Click += new System.EventHandler(this.btn_clearKeys_Click);
            // 
            // txt_template
            // 
            this.txt_template.Location = new System.Drawing.Point(15, 40);
            this.txt_template.Name = "txt_template";
            this.txt_template.Size = new System.Drawing.Size(310, 228);
            this.txt_template.TabIndex = 0;
            this.txt_template.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.windowsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(728, 24);
            this.menuStrip1.TabIndex = 25;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveTemplateToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.openToolStripMenuItem.Text = "Open template";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveTemplateToolStripMenuItem
            // 
            this.saveTemplateToolStripMenuItem.Name = "saveTemplateToolStripMenuItem";
            this.saveTemplateToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveTemplateToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.saveTemplateToolStripMenuItem.Text = "Save template";
            this.saveTemplateToolStripMenuItem.Click += new System.EventHandler(this.saveTemplateToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // clearAllToolStripMenuItem
            // 
            this.clearAllToolStripMenuItem.Name = "clearAllToolStripMenuItem";
            this.clearAllToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.clearAllToolStripMenuItem.Text = "Clear all";
            this.clearAllToolStripMenuItem.Click += new System.EventHandler(this.clearAllToolStripMenuItem_Click);
            // 
            // windowsToolStripMenuItem
            // 
            this.windowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sQLToolStripMenuItem});
            this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
            this.windowsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.windowsToolStripMenuItem.Text = "Tools";
            // 
            // sQLToolStripMenuItem
            // 
            this.sQLToolStripMenuItem.Name = "sQLToolStripMenuItem";
            this.sQLToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.sQLToolStripMenuItem.Text = "SQL builder";
            this.sQLToolStripMenuItem.Click += new System.EventHandler(this.sQLToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 485);
            this.Controls.Add(this.btn_clearKeys);
            this.Controls.Add(this.btn_clearValues);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_val8);
            this.Controls.Add(this.txt_key8);
            this.Controls.Add(this.txt_val7);
            this.Controls.Add(this.txt_key7);
            this.Controls.Add(this.txt_val6);
            this.Controls.Add(this.txt_key6);
            this.Controls.Add(this.txt_val5);
            this.Controls.Add(this.txt_key5);
            this.Controls.Add(this.txt_val4);
            this.Controls.Add(this.txt_key4);
            this.Controls.Add(this.txt_val3);
            this.Controls.Add(this.txt_key3);
            this.Controls.Add(this.txt_val2);
            this.Controls.Add(this.txt_key2);
            this.Controls.Add(this.txt_val1);
            this.Controls.Add(this.txt_key1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_console);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_template);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Syntaxer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txt_console;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txt_key1;
        private System.Windows.Forms.TextBox txt_val1;
        private System.Windows.Forms.TextBox txt_val2;
        private System.Windows.Forms.TextBox txt_key2;
        private System.Windows.Forms.TextBox txt_val3;
        private System.Windows.Forms.TextBox txt_key3;
        private System.Windows.Forms.TextBox txt_val4;
        private System.Windows.Forms.TextBox txt_key4;
        private System.Windows.Forms.TextBox txt_val5;
        private System.Windows.Forms.TextBox txt_key5;
        private System.Windows.Forms.TextBox txt_val6;
        private System.Windows.Forms.TextBox txt_key6;
        private System.Windows.Forms.TextBox txt_val7;
        private System.Windows.Forms.TextBox txt_key7;
        private System.Windows.Forms.TextBox txt_val8;
        private System.Windows.Forms.TextBox txt_key8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_clearValues;
        private System.Windows.Forms.Button btn_clearKeys;
        private System.Windows.Forms.CheckBox cbox_append;
        private System.Windows.Forms.Button btn_clearConsole;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_endWith;
        private System.Windows.Forms.TextBox txt_beginWith;
        private System.Windows.Forms.RichTextBox txt_template;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveTemplateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sQLToolStripMenuItem;
    }
}

