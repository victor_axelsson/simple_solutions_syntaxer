﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Syntaxer
{
    public partial class SQLBuilder : Form, IClickable
    {
        private List<SQLCommandPiece> cmds;
        private SQLCommandPiece valuesSql;
        public string Syntax { get; set; }

        public SQLBuilder()
        {
            InitializeComponent();

            cmds = new List<SQLCommandPiece>();
            cmds.Add(new SQLCommandPiece(this) { 
                KeyShortcut = Keys.NumPad7 | Keys.Control,
                Syntax = "INSERT ",
                CmdButton = btn_insert 
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.NumPad8 | Keys.Control,
                Syntax = "SELECT ",
                CmdButton = btn_select
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.NumPad9 | Keys.Control,
                Syntax = "UPDATE ",
                CmdButton = btn_update
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.NumPad4 | Keys.Control,
                Syntax = "DELETE ",
                CmdButton = btn_delete
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.NumPad5 | Keys.Control,
                Syntax = "* ",
                CmdButton = btn_all
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.NumPad6 | Keys.Control,
                Syntax = "FROM ",
                CmdButton = btn_from
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.NumPad1 | Keys.Control,
                Syntax = "INTO ",
                CmdButton = btn_into
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.NumPad2 | Keys.Control,
                Syntax = "WHERE ",
                CmdButton = btn_where
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.NumPad3 | Keys.Control,
                Syntax = "VALUES ",
                CmdButton = btn_values
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.NumPad0 | Keys.Control,
                Syntax = " ",
                Textbox = txt_tableName,
                CmdButton = btn_tableName
            });

            valuesSql = new SQLCommandPiece(this); 
            valuesSql.KeyShortcut = Keys.Insert | Keys.Control;
            valuesSql.Syntax = "";
            valuesSql.CmdButton = btn_valuesInsert;

            cmds.Add(valuesSql);

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.Q | Keys.Control,
                Syntax = "",
                Textbox = txt_custom1,
                CmdButton = btn_custom1
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.W | Keys.Control,
                Syntax = "",
                Textbox = txt_custom2,
                CmdButton = btn_custom2
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.E | Keys.Control,
                Syntax = "",
                Textbox = txt_custom3,
                CmdButton = btn_custom3
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.A | Keys.Control,
                Syntax = "",
                Textbox = txt_custom4,
                CmdButton = btn_custom4
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.S | Keys.Control,
                Syntax = "",
                Textbox = txt_custom5,
                CmdButton = btn_custom5
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.D | Keys.Control,
                Syntax = "",
                Textbox = txt_custom6,
                CmdButton = btn_custom6
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.Z | Keys.Control,
                Syntax = "",
                Textbox = txt_custom7,
                CmdButton = btn_custom7
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.X | Keys.Control,
                Syntax = "",
                Textbox = txt_custom8,
                CmdButton = btn_custom8
            });

            cmds.Add(new SQLCommandPiece(this)
            {
                KeyShortcut = Keys.C | Keys.Control,
                Syntax = "",
                Textbox = txt_custom9,
                CmdButton = btn_custom9
            });
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            foreach (var cmd in cmds)
            {
                if (cmd.CorresponsToKey(keyData))
                {
                    cmd.CmdButton.PerformClick();
                    break; 
                }
            }
                
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void SQLBuilder_Load(object sender, EventArgs e)
        {

        }


        public void click(SQLCommandPiece cmd)
        {
            txt_console.Text += cmd.Syntax; 
        }

        private void btn_values_Click(object sender, EventArgs e)
        {
            txt_values.Focus(); 
        }

        private void txt_values_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void btn_valuesInsert_Click(object sender, EventArgs e)
        {

            string content = txt_values.Text;
            txt_console.Text += "(" + content + ") "; 
        }

        private void button12_Click(object sender, EventArgs e)
        {
            txt_console.Text = "";
            txt_custom1.Text = "";
            txt_custom2.Text = "";
            txt_custom3.Text = "";
            txt_custom4.Text = "";
            txt_custom5.Text = "";
            txt_custom6.Text = "";
            txt_custom7.Text = "";
            txt_custom8.Text = "";
            txt_custom9.Text = "";
            txt_tableName.Text = "";
            txt_values.Text = ""; 
           
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Syntax = txt_console.Text; 
        }
    }
}
