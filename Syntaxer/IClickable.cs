﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Syntaxer
{
    public interface IClickable
    {
        void click(SQLCommandPiece cmd); 
    }
}
