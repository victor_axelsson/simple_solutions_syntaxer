﻿namespace Syntaxer
{
    partial class SQLBuilder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_insert = new System.Windows.Forms.Button();
            this.btn_select = new System.Windows.Forms.Button();
            this.btn_update = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_all = new System.Windows.Forms.Button();
            this.btn_from = new System.Windows.Forms.Button();
            this.btn_into = new System.Windows.Forms.Button();
            this.btn_where = new System.Windows.Forms.Button();
            this.btn_values = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_tableName = new System.Windows.Forms.Button();
            this.txt_tableName = new System.Windows.Forms.TextBox();
            this.txt_console = new System.Windows.Forms.RichTextBox();
            this.txt_custom1 = new System.Windows.Forms.TextBox();
            this.btn_custom1 = new System.Windows.Forms.Button();
            this.btn_custom2 = new System.Windows.Forms.Button();
            this.txt_custom2 = new System.Windows.Forms.TextBox();
            this.btn_custom3 = new System.Windows.Forms.Button();
            this.txt_custom3 = new System.Windows.Forms.TextBox();
            this.btn_custom4 = new System.Windows.Forms.Button();
            this.txt_custom4 = new System.Windows.Forms.TextBox();
            this.btn_custom5 = new System.Windows.Forms.Button();
            this.txt_custom5 = new System.Windows.Forms.TextBox();
            this.btn_custom6 = new System.Windows.Forms.Button();
            this.txt_custom6 = new System.Windows.Forms.TextBox();
            this.btn_custom7 = new System.Windows.Forms.Button();
            this.txt_custom7 = new System.Windows.Forms.TextBox();
            this.btn_custom8 = new System.Windows.Forms.Button();
            this.txt_custom8 = new System.Windows.Forms.TextBox();
            this.btn_custom9 = new System.Windows.Forms.Button();
            this.txt_custom9 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_values = new System.Windows.Forms.TextBox();
            this.btn_valuesInsert = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_insert
            // 
            this.btn_insert.Location = new System.Drawing.Point(6, 19);
            this.btn_insert.Name = "btn_insert";
            this.btn_insert.Size = new System.Drawing.Size(75, 23);
            this.btn_insert.TabIndex = 0;
            this.btn_insert.Text = "INSERT (7)";
            this.btn_insert.UseVisualStyleBackColor = true;
            // 
            // btn_select
            // 
            this.btn_select.Location = new System.Drawing.Point(87, 19);
            this.btn_select.Name = "btn_select";
            this.btn_select.Size = new System.Drawing.Size(75, 23);
            this.btn_select.TabIndex = 1;
            this.btn_select.Text = "SELECT (8)";
            this.btn_select.UseVisualStyleBackColor = true;
            // 
            // btn_update
            // 
            this.btn_update.Location = new System.Drawing.Point(168, 19);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(75, 23);
            this.btn_update.TabIndex = 2;
            this.btn_update.Text = "UPDATE (9)";
            this.btn_update.UseVisualStyleBackColor = true;
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(6, 48);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_delete.TabIndex = 3;
            this.btn_delete.Text = "DELETE (4)";
            this.btn_delete.UseVisualStyleBackColor = true;
            // 
            // btn_all
            // 
            this.btn_all.Location = new System.Drawing.Point(87, 48);
            this.btn_all.Name = "btn_all";
            this.btn_all.Size = new System.Drawing.Size(75, 23);
            this.btn_all.TabIndex = 4;
            this.btn_all.Text = "* (5)";
            this.btn_all.UseVisualStyleBackColor = true;
            // 
            // btn_from
            // 
            this.btn_from.Location = new System.Drawing.Point(168, 48);
            this.btn_from.Name = "btn_from";
            this.btn_from.Size = new System.Drawing.Size(75, 23);
            this.btn_from.TabIndex = 5;
            this.btn_from.Text = "FROM (6)";
            this.btn_from.UseVisualStyleBackColor = true;
            // 
            // btn_into
            // 
            this.btn_into.Location = new System.Drawing.Point(6, 77);
            this.btn_into.Name = "btn_into";
            this.btn_into.Size = new System.Drawing.Size(75, 23);
            this.btn_into.TabIndex = 6;
            this.btn_into.Text = "INTO (1)";
            this.btn_into.UseVisualStyleBackColor = true;
            // 
            // btn_where
            // 
            this.btn_where.Location = new System.Drawing.Point(87, 77);
            this.btn_where.Name = "btn_where";
            this.btn_where.Size = new System.Drawing.Size(75, 23);
            this.btn_where.TabIndex = 7;
            this.btn_where.Text = "WHERE (2)";
            this.btn_where.UseVisualStyleBackColor = true;
            // 
            // btn_values
            // 
            this.btn_values.Location = new System.Drawing.Point(168, 77);
            this.btn_values.Name = "btn_values";
            this.btn_values.Size = new System.Drawing.Size(75, 23);
            this.btn_values.TabIndex = 8;
            this.btn_values.Text = "VALUES (3)";
            this.btn_values.UseVisualStyleBackColor = true;
            this.btn_values.Click += new System.EventHandler(this.btn_values_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_tableName);
            this.groupBox1.Controls.Add(this.btn_insert);
            this.groupBox1.Controls.Add(this.txt_tableName);
            this.groupBox1.Controls.Add(this.btn_values);
            this.groupBox1.Controls.Add(this.btn_select);
            this.groupBox1.Controls.Add(this.btn_where);
            this.groupBox1.Controls.Add(this.btn_update);
            this.groupBox1.Controls.Add(this.btn_into);
            this.groupBox1.Controls.Add(this.btn_delete);
            this.groupBox1.Controls.Add(this.btn_from);
            this.groupBox1.Controls.Add(this.btn_all);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(252, 139);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Numpad";
            // 
            // btn_tableName
            // 
            this.btn_tableName.Location = new System.Drawing.Point(168, 106);
            this.btn_tableName.Name = "btn_tableName";
            this.btn_tableName.Size = new System.Drawing.Size(75, 23);
            this.btn_tableName.TabIndex = 9;
            this.btn_tableName.Text = "[my table] (0)";
            this.btn_tableName.UseVisualStyleBackColor = true;
            // 
            // txt_tableName
            // 
            this.txt_tableName.Location = new System.Drawing.Point(6, 106);
            this.txt_tableName.Name = "txt_tableName";
            this.txt_tableName.Size = new System.Drawing.Size(156, 20);
            this.txt_tableName.TabIndex = 11;
            // 
            // txt_console
            // 
            this.txt_console.Location = new System.Drawing.Point(12, 311);
            this.txt_console.Name = "txt_console";
            this.txt_console.Size = new System.Drawing.Size(789, 96);
            this.txt_console.TabIndex = 10;
            this.txt_console.Text = "";
            // 
            // txt_custom1
            // 
            this.txt_custom1.Location = new System.Drawing.Point(6, 19);
            this.txt_custom1.Name = "txt_custom1";
            this.txt_custom1.Size = new System.Drawing.Size(80, 20);
            this.txt_custom1.TabIndex = 11;
            // 
            // btn_custom1
            // 
            this.btn_custom1.Location = new System.Drawing.Point(92, 16);
            this.btn_custom1.Name = "btn_custom1";
            this.btn_custom1.Size = new System.Drawing.Size(78, 23);
            this.btn_custom1.TabIndex = 12;
            this.btn_custom1.Text = "[custom1] (q)";
            this.btn_custom1.UseVisualStyleBackColor = true;
            // 
            // btn_custom2
            // 
            this.btn_custom2.Location = new System.Drawing.Point(260, 16);
            this.btn_custom2.Name = "btn_custom2";
            this.btn_custom2.Size = new System.Drawing.Size(78, 23);
            this.btn_custom2.TabIndex = 14;
            this.btn_custom2.Text = "[custom2] (w)";
            this.btn_custom2.UseVisualStyleBackColor = true;
            // 
            // txt_custom2
            // 
            this.txt_custom2.Location = new System.Drawing.Point(174, 18);
            this.txt_custom2.Name = "txt_custom2";
            this.txt_custom2.Size = new System.Drawing.Size(80, 20);
            this.txt_custom2.TabIndex = 13;
            // 
            // btn_custom3
            // 
            this.btn_custom3.Location = new System.Drawing.Point(430, 16);
            this.btn_custom3.Name = "btn_custom3";
            this.btn_custom3.Size = new System.Drawing.Size(78, 23);
            this.btn_custom3.TabIndex = 16;
            this.btn_custom3.Text = "[custom3] (e)";
            this.btn_custom3.UseVisualStyleBackColor = true;
            // 
            // txt_custom3
            // 
            this.txt_custom3.Location = new System.Drawing.Point(344, 18);
            this.txt_custom3.Name = "txt_custom3";
            this.txt_custom3.Size = new System.Drawing.Size(80, 20);
            this.txt_custom3.TabIndex = 15;
            // 
            // btn_custom4
            // 
            this.btn_custom4.Location = new System.Drawing.Point(92, 45);
            this.btn_custom4.Name = "btn_custom4";
            this.btn_custom4.Size = new System.Drawing.Size(78, 23);
            this.btn_custom4.TabIndex = 18;
            this.btn_custom4.Text = "[custom4] (a)";
            this.btn_custom4.UseVisualStyleBackColor = true;
            // 
            // txt_custom4
            // 
            this.txt_custom4.Location = new System.Drawing.Point(6, 47);
            this.txt_custom4.Name = "txt_custom4";
            this.txt_custom4.Size = new System.Drawing.Size(80, 20);
            this.txt_custom4.TabIndex = 17;
            // 
            // btn_custom5
            // 
            this.btn_custom5.Location = new System.Drawing.Point(260, 45);
            this.btn_custom5.Name = "btn_custom5";
            this.btn_custom5.Size = new System.Drawing.Size(78, 23);
            this.btn_custom5.TabIndex = 20;
            this.btn_custom5.Text = "[custom5] (s)";
            this.btn_custom5.UseVisualStyleBackColor = true;
            // 
            // txt_custom5
            // 
            this.txt_custom5.Location = new System.Drawing.Point(174, 47);
            this.txt_custom5.Name = "txt_custom5";
            this.txt_custom5.Size = new System.Drawing.Size(80, 20);
            this.txt_custom5.TabIndex = 19;
            // 
            // btn_custom6
            // 
            this.btn_custom6.Location = new System.Drawing.Point(430, 47);
            this.btn_custom6.Name = "btn_custom6";
            this.btn_custom6.Size = new System.Drawing.Size(78, 23);
            this.btn_custom6.TabIndex = 22;
            this.btn_custom6.Text = "[custom6] (d)";
            this.btn_custom6.UseVisualStyleBackColor = true;
            // 
            // txt_custom6
            // 
            this.txt_custom6.Location = new System.Drawing.Point(344, 49);
            this.txt_custom6.Name = "txt_custom6";
            this.txt_custom6.Size = new System.Drawing.Size(80, 20);
            this.txt_custom6.TabIndex = 21;
            // 
            // btn_custom7
            // 
            this.btn_custom7.Location = new System.Drawing.Point(92, 73);
            this.btn_custom7.Name = "btn_custom7";
            this.btn_custom7.Size = new System.Drawing.Size(78, 23);
            this.btn_custom7.TabIndex = 24;
            this.btn_custom7.Text = "[custom7] (z)";
            this.btn_custom7.UseVisualStyleBackColor = true;
            // 
            // txt_custom7
            // 
            this.txt_custom7.Location = new System.Drawing.Point(6, 75);
            this.txt_custom7.Name = "txt_custom7";
            this.txt_custom7.Size = new System.Drawing.Size(80, 20);
            this.txt_custom7.TabIndex = 23;
            // 
            // btn_custom8
            // 
            this.btn_custom8.Location = new System.Drawing.Point(260, 75);
            this.btn_custom8.Name = "btn_custom8";
            this.btn_custom8.Size = new System.Drawing.Size(78, 23);
            this.btn_custom8.TabIndex = 26;
            this.btn_custom8.Text = "[custom8] (x)";
            this.btn_custom8.UseVisualStyleBackColor = true;
            // 
            // txt_custom8
            // 
            this.txt_custom8.Location = new System.Drawing.Point(174, 77);
            this.txt_custom8.Name = "txt_custom8";
            this.txt_custom8.Size = new System.Drawing.Size(80, 20);
            this.txt_custom8.TabIndex = 25;
            // 
            // btn_custom9
            // 
            this.btn_custom9.Location = new System.Drawing.Point(430, 75);
            this.btn_custom9.Name = "btn_custom9";
            this.btn_custom9.Size = new System.Drawing.Size(78, 23);
            this.btn_custom9.TabIndex = 28;
            this.btn_custom9.Text = "[custom9] (c)";
            this.btn_custom9.UseVisualStyleBackColor = true;
            // 
            // txt_custom9
            // 
            this.txt_custom9.Location = new System.Drawing.Point(344, 77);
            this.txt_custom9.Name = "txt_custom9";
            this.txt_custom9.Size = new System.Drawing.Size(80, 20);
            this.txt_custom9.TabIndex = 27;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_custom1);
            this.groupBox2.Controls.Add(this.btn_custom9);
            this.groupBox2.Controls.Add(this.btn_custom1);
            this.groupBox2.Controls.Add(this.txt_custom9);
            this.groupBox2.Controls.Add(this.txt_custom2);
            this.groupBox2.Controls.Add(this.btn_custom8);
            this.groupBox2.Controls.Add(this.btn_custom2);
            this.groupBox2.Controls.Add(this.txt_custom8);
            this.groupBox2.Controls.Add(this.txt_custom3);
            this.groupBox2.Controls.Add(this.btn_custom7);
            this.groupBox2.Controls.Add(this.btn_custom3);
            this.groupBox2.Controls.Add(this.txt_custom7);
            this.groupBox2.Controls.Add(this.txt_custom4);
            this.groupBox2.Controls.Add(this.btn_custom6);
            this.groupBox2.Controls.Add(this.btn_custom4);
            this.groupBox2.Controls.Add(this.txt_custom6);
            this.groupBox2.Controls.Add(this.txt_custom5);
            this.groupBox2.Controls.Add(this.btn_custom5);
            this.groupBox2.Location = new System.Drawing.Point(270, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(515, 115);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Keyboard";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 193);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 63);
            this.label1.TabIndex = 30;
            this.label1.Text = "(";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(395, 193);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 63);
            this.label2.TabIndex = 31;
            this.label2.Text = ")";
            // 
            // txt_values
            // 
            this.txt_values.Location = new System.Drawing.Point(42, 219);
            this.txt_values.Name = "txt_values";
            this.txt_values.Size = new System.Drawing.Size(363, 20);
            this.txt_values.TabIndex = 32;
            this.txt_values.TextChanged += new System.EventHandler(this.txt_values_TextChanged);
            // 
            // btn_valuesInsert
            // 
            this.btn_valuesInsert.Location = new System.Drawing.Point(428, 217);
            this.btn_valuesInsert.Name = "btn_valuesInsert";
            this.btn_valuesInsert.Size = new System.Drawing.Size(128, 23);
            this.btn_valuesInsert.TabIndex = 12;
            this.btn_valuesInsert.Text = "[values] (Insert)";
            this.btn_valuesInsert.UseVisualStyleBackColor = true;
            this.btn_valuesInsert.Click += new System.EventHandler(this.btn_valuesInsert_Click);
            // 
            // button11
            // 
            this.button11.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button11.Location = new System.Drawing.Point(12, 414);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 12;
            this.button11.Text = "Import";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(93, 414);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 33;
            this.button12.Text = "Clear";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // SQLBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 449);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.btn_valuesInsert);
            this.Controls.Add(this.txt_values);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txt_console);
            this.Controls.Add(this.groupBox1);
            this.Name = "SQLBuilder";
            this.Text = "SQLBuilder";
            this.Load += new System.EventHandler(this.SQLBuilder_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_insert;
        private System.Windows.Forms.Button btn_select;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_all;
        private System.Windows.Forms.Button btn_from;
        private System.Windows.Forms.Button btn_into;
        private System.Windows.Forms.Button btn_where;
        private System.Windows.Forms.Button btn_values;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox txt_console;
        private System.Windows.Forms.TextBox txt_tableName;
        private System.Windows.Forms.Button btn_tableName;
        private System.Windows.Forms.TextBox txt_custom1;
        private System.Windows.Forms.Button btn_custom1;
        private System.Windows.Forms.Button btn_custom2;
        private System.Windows.Forms.TextBox txt_custom2;
        private System.Windows.Forms.Button btn_custom3;
        private System.Windows.Forms.TextBox txt_custom3;
        private System.Windows.Forms.Button btn_custom4;
        private System.Windows.Forms.TextBox txt_custom4;
        private System.Windows.Forms.Button btn_custom5;
        private System.Windows.Forms.TextBox txt_custom5;
        private System.Windows.Forms.Button btn_custom6;
        private System.Windows.Forms.TextBox txt_custom6;
        private System.Windows.Forms.Button btn_custom7;
        private System.Windows.Forms.TextBox txt_custom7;
        private System.Windows.Forms.Button btn_custom8;
        private System.Windows.Forms.TextBox txt_custom8;
        private System.Windows.Forms.Button btn_custom9;
        private System.Windows.Forms.TextBox txt_custom9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_values;
        private System.Windows.Forms.Button btn_valuesInsert;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
    }
}