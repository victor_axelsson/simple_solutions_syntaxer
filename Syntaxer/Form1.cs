﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Syntaxer
{
    public partial class Form1 : Form
    {
        List<ContentValue> contentValues;
        string currTemplate; 

        public Form1()
        {
            InitializeComponent();

            contentValues = new List<ContentValue>();
            currTemplate = null; 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RefreshValues();

            string code = txt_template.Text;

            for (int i = 0; i < contentValues.Count; i++)
            {
                if (contentValues[i].Content.Length > 0)
                {
                    code = code.Replace(contentValues[i].Content, contentValues[i].Values);
                }
            }

            code = appendRowBreaks(code);

            if (cbox_append.Checked)
            {
                txt_console.Text += code + "\n\n";
            }
            else
            {
                txt_console.Text = code + "\n\n";
            }
        }

        private string appendRowBreaks(string code)
        {
            if (txt_beginWith.Text.Length > 0)
            {
                code = code.Replace("\n", "\n" + txt_beginWith.Text);
                code = txt_beginWith.Text + code; 

            }

            if (txt_endWith.Text.Length > 0)
            {
                code = code.Replace("\n", txt_endWith.Text + "\n");
                code = code + txt_endWith.Text; 
            }



            return code; 
        }

        private void clearKeys()
        {
            txt_key1.Text = "";
            txt_key2.Text = "";
            txt_key3.Text = "";
            txt_key4.Text = "";
            txt_key5.Text = "";
            txt_key6.Text = "";
            txt_key7.Text = "";
            txt_key8.Text = "";
        }

        private void clearVals()
        {
            txt_val1.Text = "";
            txt_val2.Text = "";
            txt_val3.Text = "";
            txt_val4.Text = "";
            txt_val5.Text = "";
            txt_val6.Text = "";
            txt_val7.Text = "";
            txt_val8.Text = "";
        }

        private void RefreshValues()
        {
            ContentValue val1 = new ContentValue();
            val1.Content = txt_key1.Text;
            val1.Values = txt_val1.Text;

            ContentValue val2 = new ContentValue();
            val2.Content = txt_key2.Text;
            val2.Values = txt_val2.Text;


            ContentValue val3 = new ContentValue();
            val3.Content = txt_key3.Text;
            val3.Values = txt_val3.Text;

            ContentValue val4 = new ContentValue();
            val4.Content = txt_key4.Text;
            val4.Values = txt_val4.Text;

            ContentValue val5 = new ContentValue();
            val5.Content = txt_key5.Text;
            val5.Values = txt_val5.Text;

            ContentValue val6 = new ContentValue();
            val6.Content = txt_key6.Text;
            val6.Values = txt_val6.Text;

            ContentValue val7 = new ContentValue();
            val7.Content = txt_key7.Text;
            val7.Values = txt_val7.Text;

            ContentValue val8 = new ContentValue();
            val8.Content = txt_key8.Text;
            val8.Values = txt_val8.Text;

            contentValues = new List<ContentValue>(); 
            contentValues.Add(val1);
            contentValues.Add(val2);
            contentValues.Add(val3);
            contentValues.Add(val4);
            contentValues.Add(val5);
            contentValues.Add(val6);
            contentValues.Add(val7);
            contentValues.Add(val8); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clearVals(); 
        }

        private void btn_clearKeys_Click(object sender, EventArgs e)
        {
            clearKeys(); 
        }

        private void btn_clearConsole_Click(object sender, EventArgs e)
        {
            txt_console.Text = ""; 
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.Stream fileStream = dialog.OpenFile();
                currTemplate = dialog.FileName;


                using (System.IO.StreamReader reader = new System.IO.StreamReader(fileStream))
                {
                    // Read the first line from the file and write it the textbox.
                    txt_template.Text = reader.ReadToEnd(); 
                }
                fileStream.Close();
            }
        }

        private void saveTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.File.WriteAllText(dialog.FileName, txt_template.Text);
            }

        }

        private void clearAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clearAll(); 
        }

        private void clearAll() 
        {
            clearKeys();
            clearVals();
            txt_template.Text = "";
            txt_console.Text = "";
            txt_beginWith.Text = "";
            txt_endWith.Text = "";
            currTemplate = null; 
        }

        private void sQLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SQLBuilder sql = new SQLBuilder();
            if (sql.ShowDialog() == DialogResult.OK)
            {
                txt_template.Text += sql.Syntax; 
            }
        }
    }
}
