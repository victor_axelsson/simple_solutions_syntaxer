﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Syntaxer
{
    public class SQLCommandPiece
    {
        private Keys keyShortcut;
        private string syntax;
        private Button cmdButton;
        private IClickable clicker;
        private TextBox textbox;

        public SQLCommandPiece(IClickable clicker)
        {
            this.clicker = clicker;
        }
        
        public TextBox Textbox
        {
            get { return textbox; }
            set { textbox = value; }
        }

        public Button CmdButton
        {
            get { return cmdButton; }
            set { 
                cmdButton = value;
                cmdButton.Click += cmdButton_Click;
            }
        }

        void cmdButton_Click(object sender, EventArgs e)
        {
            clicker.click(this); 
        }

        public string Syntax
        {
            get {
                return textbox == null ? syntax : textbox.Text + " "; 
            }
            set { syntax = value; }
        }

        public Keys KeyShortcut
        {
            get { return keyShortcut; }
            set { keyShortcut = value; }
        }

        public bool CorresponsToKey(Keys k)
        {
            return k == keyShortcut; 
        }

    }
}
