﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Syntaxer
{
    public class ContentValue
    {

        private string content;
        private string value;

        public string Values
        {
            get { return value; }
            set { this.value = value; }
        }


        public string Content
        {
            get { return content; }
            set { content = value; }
        }
        
    }
}
